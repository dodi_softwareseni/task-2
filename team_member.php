<?php
/*
Plugin Name: Team Member
Plugin URI: http://wp.tutsplus.com/
Description: Declares a plugin that will create a custom post type displaying movie reviews.
Version: 1.0
Author: Soumitra Chakraborty
Author URI: http://wp.tutsplus.com/
License: GPLv2
*/

function team_member() {
    register_post_type( 'team_member_reviews',
        array(
            'labels' => array(
                'name' => 'Team Members',
                'singular_name' => 'Team Member Review',
                'add_new' => 'Add New',
                'add_new_item' => 'Add New Team Member',
                'edit' => 'Edit',
                'edit_item' => 'Edit Team Member Review',
                'new_item' => 'New Team Member Review',
                'view' => 'View',
                'view_item' => 'View Team Member Review',
                'search_items' => 'Search Team Member Reviews',
                'not_found' => 'No Team Member found',
                'not_found_in_trash' => 'No Movie Team Member found in Trash',
                'parent' => 'Parent Team Member Review'
            ),
 
            'public' => true,
            'menu_position' => 15,
            'supports' => array( 'title', 'editor', 'comments', 'thumbnail', 'custom-fields' ),
            'taxonomies' => array( '' ),
            'menu_icon' => plugins_url( 'images/image.png', __FILE__ ),
            'has_archive' => true
        )
    );
}

add_action( 'init', 'team_member' );

add_filter( 'rwmb_meta_boxes', 'team_member_meta_boxes' );

function team_member_meta_boxes( $meta_boxes ) {
    $meta_boxes[] = array(
        'title'      => __( 'Other Information', 'textdomain' ),
        'post_types' => 'team_member_reviews',
        'fields'     => array(
            array(
                'id'   => 'position',
                'name' => __( 'Position', 'textdomain' ),
                'type' => 'text',
            ),
            array(
                'id'   => 'email',
                'name' => __( 'Email', 'textdomain' ),
                'type' => 'email',
            ),
            array(
                'id'   => 'phone',
                'name' => __( 'Phone', 'textdomain' ),
                'type' => 'text',
            ),
            array(
                'id'   => 'website',
                'name' => __( 'Website', 'textdomain' ),
                'type' => 'text',
            ),
            array(
                'id'   => "image",
                'name' => __( 'Image', 'textdomain' ),
                'type' => 'image',
            ),
        ),
    );
    return $meta_boxes;
}


function team_member_shortcode($atts){
    ob_start();
    $args = array(
        'post_type' => 'team_member_reviews',
    );
    $the_query = new WP_Query( $args );

    // $mypost = array( 'post_type' => 'team_member_reviews', );
    // $loop = new WP_Query( $mypost );

    if ( $the_query->have_posts() ) {
        echo '<ul>';
        while ( $the_query->have_posts() ) {
            $the_query->the_post();
            $imageId = get_post_meta(get_the_ID(), 'image', true);

            ?>
                <div style="background: #1E90FF;width:30%;padding:0 50pt 0 0;float:left;">
                    <br>
                    <?php echo wp_get_attachment_image($imageId , $size= 'thumbnail', $icon = false, $attr= '');?>
                    <br><br>
                    <strong><?php echo get_the_title();?></strong>
                    <?php echo get_post_meta(get_the_ID(), 'position', true) ;?><br>
                    <?php echo get_post_meta(get_the_ID(), 'email', true) ;?><br>
                    <?php echo get_post_meta(get_the_ID(), 'phone', true) ;?><br>
                    <?php echo get_post_meta(get_the_ID(), 'website', true) ;?><br>
                </div>
            <?php

        }
        echo '</ul>';
        wp_reset_postdata();
    } else {
        echo  "no posts found";
    }

    return ob_get_clean();
}

add_shortcode('team_member_name', 'team_member_shortcode');

function team_member_shortcode_display( $atts ) {
    ob_start();
    $atts = shortcode_atts( array(
        'email' => 'n',
        'phone' => 'n',
        'website' => 'n'
    ), $atts, 'team_member_display' );

    $args = array(
        'post_type' => 'team_member_reviews',
    );
    $the_query = new WP_Query( $args );

    if ( $the_query->have_posts() ) {
        echo '<ul>';
        while ( $the_query->have_posts() ) {
            $the_query->the_post();



            if($atts['email'] == 'y'){
                ?>
                    <?php echo get_post_meta(get_the_ID(), 'email', true) ;?><br>
                <?php
            }
            
            if($atts['phone'] == 'y'){
                ?>
                    <?php echo get_post_meta(get_the_ID(), 'phone', true) ;?><br>
                <?php
            }
            

            if($atts['website'] == 'y'){
                ?>
                    <?php echo get_post_meta(get_the_ID(), 'website', true) ;?><br>
                <?php
            }


                }
        echo '</ul>';
        wp_reset_postdata();
    } else {
        echo  "no posts found";
    }

    return ob_get_clean();
}

add_shortcode( 'team_member_display', 'team_member_shortcode_display' );


?>